package org.localomaha;

public class LocalBusiness {
	private String address, name;
	private String category;

	public String toString() {
		return name + " " + address;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setCategory(String text) {
		this.category=text;
	}

	public String getCategory() {
		return category;
	}

}
