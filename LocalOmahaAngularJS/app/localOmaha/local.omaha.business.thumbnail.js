'use strict';

angular.module('localOmaha').directive('localOmahaBusinessThumbnail', function() {
    return {
        restrict: 'E',
        templateUrl: 'localOmaha/local.omaha.business.thumbnail.html',
        scope: {
            business: '=',
            userLocation: '=',
            userDeviceOrientation: '=',
            showCompass: '&'
        },
        controller: function($scope) {
            var businessLocation = {lat: $scope.business.lat, lon: $scope.business.lng};

            $scope.getDistance = function() {
                if ($scope.userLocation) {
                    $scope.units = 'miles';
                    $scope.business.distance =  getDistanceFromCoords($scope.userLocation, businessLocation);
                    return $scope.business.distance;
                }
            };

            $scope.getDirection = function() {
                if ($scope.userLocation) {
                    $scope.business.showDirection = true;
                    $scope.business.direction = getBearingFromCoords($scope.userLocation, businessLocation) - $scope.userDeviceOrientation;
                    return $scope.business.direction;
                }
            };
        }
    };
});