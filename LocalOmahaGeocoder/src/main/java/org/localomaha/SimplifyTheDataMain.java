package org.localomaha;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.TypeFactory;

public class SimplifyTheDataMain {

	public static void main(String[] args) {
		try {
			new SimplifyTheDataMain().run();
			System.out.println("Normal Termination.");
		} catch (Exception bland) {
			bland.printStackTrace();
		}

	}

	public void run() throws JsonParseException, JsonMappingException,
			IOException {
		ObjectMapper mapper = new ObjectMapper();
		String directoryName = "src/main/resources/";
		String inputFileName = directoryName + "75withLatLong.json";
		String outputFileName = directoryName + "simple75withLatLong.json";
		Random rand = new Random();
		String[] fakeCategory = { "Bar", "Bookstore", "Bakery", "Bank",
				"Beauty", "Dining" };
		int min = 0, max = fakeCategory.length - 1;

		/*
		 * Below fails like so: org.codehaus.jackson.map.JsonMappingException:
		 * No suitable constructor found for type [simple type, class
		 * com.google.maps.model.LatLng]: can not instantiate from JSON object
		 * (need to add/enable type information?) at [Source:
		 * src/main/resources/75withLatLong.json; line: 1, column: 976] (through
		 * reference chain:
		 * org.localomaha.LocalBusiness["geoCodingResults"]->com
		 * .google.maps.model
		 * .GeocodingResult["geometry"]->com.google.maps.model.
		 * Geometry["location"]) at
		 * org.codehaus.jackson.map.JsonMappingException
		 * .from(JsonMappingException.java:163)
		 * 
		 * 
		 * 
		 * thinking the google geocode stuff can be written out easily but not
		 * read in easily.
		 */

		// http://stackoverflow.com/questions/7246157/how-to-parse-a-json-string-to-an-array-using-jackson
		TypeFactory typeFactory = mapper.getTypeFactory();
		List<LocalBusiness> lstLocalBusiness = mapper.readValue(new File(
				inputFileName), typeFactory.constructCollectionType(List.class,
				LocalBusiness.class));
		List<LocalBusinessSimple> lstSimpleLocalBusiness = new ArrayList<LocalBusinessSimple>();
		for (LocalBusiness localBusiness : lstLocalBusiness) {

			LocalBusinessSimple simpleLocalBusiness = new LocalBusinessSimple();
			int randomNum = rand.nextInt((max - min) + 1) + min;
			simpleLocalBusiness.setCategory(fakeCategory[randomNum]);
			simpleLocalBusiness
					.setLat(localBusiness.getGeoCodingResults()[0].geometry.location.lat);
			simpleLocalBusiness
					.setLng(localBusiness.getGeoCodingResults()[0].geometry.location.lng);
			simpleLocalBusiness
					.setAddress(localBusiness.getGeoCodingResults()[0].formattedAddress);
			simpleLocalBusiness.setNumber(localBusiness.getNumber());
			simpleLocalBusiness.setName(localBusiness.getName());

			lstSimpleLocalBusiness.add(simpleLocalBusiness);
		}
		mapper.writeValue(new File(outputFileName), lstSimpleLocalBusiness);
	}
}
