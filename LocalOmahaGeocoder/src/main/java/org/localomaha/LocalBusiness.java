package org.localomaha;

import com.google.maps.model.GeocodingResult;

public class LocalBusiness {
	private int number;
	private String address, name;
	private GeocodingResult[] geoCodingResults;

	public String toString() {
		return name + " " + address;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GeocodingResult[] getGeoCodingResults() {
		return geoCodingResults;
	}

	public void setGeoCodingResults(GeocodingResult[] geoCodingResults) {
		this.geoCodingResults = geoCodingResults;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

}
